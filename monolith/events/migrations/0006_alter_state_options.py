# Generated by Django 4.0.3 on 2023-02-23 19:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_alter_state_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='state',
            options={'ordering': ('abbreviation',)},
        ),
    ]
