import Nav from './Nav';
// import {
//   BrowserRouter,
//   Routes,
//   Route
// } from 'react-router-dom'
import ConferenceForm from './ConferenceForm'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import AttendConference from './AttendConference'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        {/* <BrowserRouter>
          <Routes>
            <Route path="/home" element={}/>
          </Routes>
        </BrowserRouter> */}
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees}/> */}
        {/* <AttendConference /> */}
      </div>
      {/* <ConferenceForm /> */}
    </>
  );
}
export default App;
