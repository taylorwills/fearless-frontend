import React, { useEffect, useState } from 'react';


function LocationForm () {
  const initialState = {
    name: "",
    room_count: "",
    city: "",
    state: "",
  }
  const [states, setStates] = useState([]);

  const [formData, setFormData] = useState(initialState)

  const handleChanger = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    })
  };

  // fetching state data to populate on drop down menu
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states)
      }
  };

  // envoking fetchData after everything has rendered (useEffect)
  useEffect(() => {
     fetchData();
  }, []);

  // Posting information within form to url location
  const handleSubmit = async (e) => {
    e.preventDefault()

    console.log(formData)
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      setFormData(initialState);
    }
  }

  // JSX code that renders into HTML
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleChanger} value={formData.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"></input>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChanger} value={formData.room_count} placeholder="Room count" required type="number" id="room_count" name="room_count" className="form-control"></input>
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChanger} value={formData.city} placeholder="City" required type="text" id="city" name="city" className="form-control"></input>
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChanger} value={formData.state} required name="state" id="state" className="form-select">
                <option value="">Choose a state</option>
                {states.map(state => {
                  return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;
