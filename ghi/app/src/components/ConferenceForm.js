import { useState, useEffect } from 'react'

function ConferenceForm () {
  const initialState = {
    name: "",
    starts: "",
    ends: "",
    description: "",
    max_presentations: "",
    max_attendees: "",
    location: "",
  }
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState(initialState);

  const handleChanger = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    })
  };

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
      }
  };

  useEffect(() => {
    fetchData();
 }, []);

 const handleSubmit = async (e) => {
  e.preventDefault()

  const conferenceUrl = 'http://localhost:8000/api/conferences/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(formData),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(conferenceUrl, fetchConfig);
  if (response.ok) {
    const newConference = await response.json();
    setFormData(initialState);
    // console.log(formData)
  }
}
  // console.log(formData)

  return <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleChanger} value={formData.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChanger} value={formData.starts} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChanger} value={formData.ends} placeholder="Ends" required type="date" id="ends" name="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <textarea onChange={handleChanger} value={formData.description} placeholder="Description" className="form-control" required type ="text" id="description" name="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChanger} value={formData.max_presentations} placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChanger} value={formData.max_attendees} placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleChanger} value={formData.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  </>
}
export default ConferenceForm
