window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement('option');
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    const spinnerTag = document.querySelector(".spinner-grow");
    spinnerTag.classList.add("d-none")
    selectTag.classList.remove("d-none")

  }

  const formTag = document.getElementById("create-attendee-form"); //Get the attendee form element by its id
  formTag.addEventListener('submit', async () => { //Add an event handler for the submit event
    event.preventDefault();//Prevent the default from happening
    const formData = new FormData(formTag)//Create a FormData object from the form
    const json = JSON.stringify(Object.fromEntries(formData));//Get a new object from the form data's entries

    const attendeeUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {  //Create options for the fetch
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {

      formTag.reset();
      const newLocation = await response.json();

      const alertTag = document.querySelector('.alert');
      formTag.classList.add("d-none");
      alertTag.classList.remove("d-none");
    }
  })

});
