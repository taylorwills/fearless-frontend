function createCard(name, description, pictureUrl, date, location) {
  return `
    <div class="col">
      <div class="card col shadow mb-2 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle text-muted">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${date}</div>
      </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {

    } else {
      const data = await response.json();
      console.log(data.conferences);

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const endDate = new Date(details.conference.ends);
          const date = `${startDate.toLocaleDateString()} - ${endDate.toLocaleDateString()}`;
          const location = details.conference.location.name;
          const html = createCard(name, description, pictureUrl, date, location);
          const htmlTag = document.querySelector('.row');
          htmlTag.innerHTML += html;
          console.log(details)
        }
      }

    }
  } catch (e) {
    console.error(e);

  }

});
